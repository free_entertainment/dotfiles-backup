execute pathogen#infect()
filetype indent plugin on

syntax on
set autoindent
set mouse=a
set number
set relativenumber
set shiftwidth=2
set softtabstop=2
set expandtab

imap jj <Esc>

let &t_SI = "\e[3 q"
let &t_EI = "\e[1 q"
